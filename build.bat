@echo off


pdflatex -output-directory output .\main.tex 
makeglossaries -d output main 
pdflatex -output-directory output .\main.tex 
cd output
biber .\main
cd ..
pdflatex -output-directory output .\main.tex 