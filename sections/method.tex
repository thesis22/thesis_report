\section{Method}

\subsection{Material}
 
The smartphone used in this thesis was a Samsung s9 which contains a plethora different sensors. The sensors used for this thesis was an acceleromenter to capture accelerations of the the phone, a gyroscope for checking changes in orientation of the phone, and a gravity sensor which was used to be able to know which axis of the accelerations was affected by the gravitational acceleration force. The sensors used from the smartphone and their update frequencies can be seen in Table \ref{tab:sensors}.

\begin{table}[ht]
    \caption{Class prediction statistics for test set after training.}
    \label{tab:sensors}
    \centering
    \begin{tabular}[t]{ll}
        \hline
        Sensor name              & Frequency \\
        \hline
        LSM6DSL Acceleration Sensor & \unit[500]{Hz}    \\
        LSM6DSL Gyroscope Sensor    & \unit[500]{Hz}    \\
        Gravity Sensor      & \unit[100]{Hz}   \\
        \hline
    \end{tabular}
\end{table}

The hardware which was used to serve the deployed \gls{ml} models on a backend server was a \gls{rpi} model 3b+.

\subsection{Data collection}

The data that was used for training, validating and testing the \gls{ml} algorithms was collected during the project.
An application was developed for the smartphone which collected data for different activities.
Figure \ref{fig:data_collection} show how the data collection application looked like.
Each data collection session started with the user manually enabling all the available sensors on the smartphone which should 
collect data during the session. Then the user would start which kind of data collection session should be started out of 

\begin{itemize}
    \item Running, where the data collector user is expected to either run or jog.
    \item Cycling, where the user is expected to cycle, this could be either paddling on the cycle either standing or sitting, or even just sitting on the bike without paddling. This for convencience for the user to have an entire cycle session as the same data collection type without having to interact with the smartphone during the session. 
    \item Walking, where the user is expected to walk in a leisurely pace.
    \item Speed walking, where the user is expected to walk in a fact pace which should eventually end up the user to start breathing heavily.
\end{itemize}

For the data collection sessions, the user placed the smartphone in the right front pocket during walking, speed walking and cycling, and mounting the smarpthone on the right upper arm during running (for mounting convenience during running).

In a session, the application would record the data for all the selected sensors in 2 second windows. After the data collection time window, all of the collected sensorvalues were stored in a local \gls{db} on the smartphone
for later consumption. The \gls{db} consisted of four types of entities:

\begin{itemize}
    \item Sensor, contained the name of the sensor according to the smartphones operating system and an identifying index,
    \item SensorReading, which represented each individual sensor readings by the application. Each sensor reading was identified by a unique id. Each sensor reading was also accompanies by a timestamp for when the sensor reading occured in unix time, which sensor type it was, its value, the index of the value in cases where the sensor could read several different indices (e.g. x-, y-, and z-readings for an acceleromenter), which session and session sequence (one session sequence is one 2 second window and a session is an entire data collection session) it belonged to. 
    \item Session, which is an identifier for each individual data collection sensor which was described with an unique id and a session type.
    \item SensorSessionType, which described each type of unique data collection types, i.e. cycling, running, walking, and speed walking. Was identified by an unique id and a name corresponding to the session.
\end{itemize}

The \gls{er} diagram of the \gls{db} can be seen in Figure \ref{fig:database}.

\begin{figure}[H]
    \includegraphics[width=0.5\textwidth]{images/raster/data_collection.jpg}
    \centering
    \caption{The data collection application shows all the sensors existing on the smartphone.
    The sensor data was tagged with an activity manually entered by the user.}
    \label{fig:data_collection}
\end{figure}

\begin{figure}[h]
    \includegraphics[width=0.5\textwidth]{images/raster/database.png}
    \centering
    \caption{The data collection \gls{db} structure. The figure shows the different data entities (rectangles), their attributes (ellipsis) and their connections (parallelogram).}
    \label{fig:database}
\end{figure}

\subsection{Data processing}

\subsubsection{Data preprocessing}

Before the data could be used for training the \gls{ml} models, they needed to be aligned, cleaned, and transformed to be proper inputs to the models. \gls{ml} models usually expects inputs of a given size, and usually the same size of the different input types when dealing with multivariate inputs as in this case with several sensor readings.
To create such inputs, the data that was stored in the \gls{db} was first extracted and put in a structured tabelformat. A tabel format, such as in excel, is excelent for handling \gls{ml} data as it is easy to enforce the correct data shape, i.e. the correct number of rows and columns. The different sensor readings for a single sensor session sequence was joined in a common table, where the location (the row) of each sensor reading value was determined by its timestamp, oldest values first and the newest at the end.
Since the different sensors had different update frequencies, they populated the excel sheet at different frequencies. To make sure that there were no gaps in the data, i.e. that multiple rows and columns didn't contain empty cells, the data cleaned by filling the missing values by interpolating the missing data linearly from the two closest data points.

Since the acceleration sensor estimates the smartphones acceleration by measuring the forces which is applied to the smartphone, it will always contain readings that comes from the gravitational force. To normalize the acceleration readings to only measure the acceleration of the smartphone relative to the ground, the gravity readings was subtracted from the accelerations,

\begin{equation}
    a_{i}^{out} = a_{i}^{in} - g_{i},\, \forall i \in \{x, y, z\}
\end{equation}

where $a_{x,y,z}^{in,out}$ is the accelerations readings before and after the gravity transform for the $x$, $y$, and $z$ axes and $g_{x,y,z}$ is the gravity sensor readings for the same axes. Another important feature was to remove dependency of the orientation of the smartphone, i.e. that the model should be invariant which direction the device was held.
This was important to not introduce too much bias in the data, i.e. during the data collection, the smartphone is naturally oriented slightly different depending on the activity. In these cases the model would only learn based on the 
orientation of the smartphone and not form the dynamics of the sensor readings. To make the readings invariant under smartphone rotation\footnote{The model should only be invariant under intital rotation of the smartphone, not invariant to changes of the rotation which the gyroscope captures.} the readings was normalized by,

\begin{equation}
    a_{norm} = \sqrt{\sum\nolimits_{i \in \{x, y, z\}} a_{i}^2},
\end{equation}

and 

\begin{equation}
    \gamma_{norm} = \sqrt{\sum\nolimits_{i \in \{x, y, z\}} \gamma_{i}^2},
\end{equation}

where $a_{norm}$ is the normalized acceleration readings, $\gamma_{norm}$ is the normalized gyroscope readings and $\gamma_{x,y,z}$ are the gyroscope readings in the $x$, $y$, and $z$ axes. The final transformed sensor readings to the \gls{ml} model was,


\begin{equation}
    I =
    \left\{
        \begin{array}{cc}
            a_{norm}(t)\\
            \gamma_{norm}(t)
        \end{array}
    \right\}
      \, \forall \, t \in \{t\,|\, t >= 0 \land t < 1200\}
\end{equation}


\subsubsection{Machine learning training process}

Splitting the dataset\\
Training the model\\
Evaluating the model\\ 
Such as confusion matrix\\
Different evaluation scores, such as recall and precisison and their implications\\

\subsubsection{Machine learning deployment, monitoring and inference}

Containerize the model\\
RESTapi on raspberry pi\\
data preproceeing on rpi\\


\subsection{Sequence classifier - CNN}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{images/raster/model.png}
    \caption{The layers of the cnn classifier model.}
    \label{fig:model_layers}
\end{figure}

\subsection{Sequence reconstruction - CNN Autoencoder}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{images/raster/autoencoder.png}
    \caption{The layers of the cnn autoencoder model.}
    \label{fig:autoencoder_layers}
\end{figure}

\newpage
